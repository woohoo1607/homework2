import {fightEventListener} from "../services/fightEventListener";
import Fighter from "./Fighter";
import {IFighterDetails} from "../../types/interfaces";
import {FighterPosition} from "../../types/types";

export async function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails): Promise<Fighter> {
  return new Promise( (resolve: Function) => {
    // resolve the promise with the winner when fight is over
    const fighter1 = new Fighter({...firstFighter}, `${FighterPosition.left}-fighter-indicator`);
    const fighter2 = new Fighter({...secondFighter}, `${FighterPosition.right}-fighter-indicator`);

    let pressed: Set<string> = new Set();
    fightEventListener(resolve, pressed, fighter1, fighter2);

    document.addEventListener('keyup', function(event) {
      pressed.delete(event.code);
    });

  });
}

function getRandom(min: number ,max: number): number {
  return Math.random()*(max-min)+min
}

export function getDamage(attacker: Fighter, defender: Fighter): number {
  // return damage
  const damage = getHitPower(attacker)-getBlockPower(defender);
  return damage>0 ? damage : 0
}

export function getHitPower(fighter: Fighter): number {
  return fighter.attack*getRandom(1,2)
  // return hit power
}

export function getBlockPower(fighter: Fighter): number {
  return fighter.defense*getRandom(1,2)
  // return block power
}

export function getCriticalHit(fighter: Fighter): number {
  return fighter.attack*2
}


export function getHealthPercents(fullHealth: number, nowHealth: number): string {
  let percents = (100/fullHealth)*nowHealth;
  return percents>0 ? percents+'%' : '0%';
}

export function isCriticalHit(pressed: Set<string>, combination:[string, string, string]): boolean {
  for (let code of combination) {
    if (!pressed.has(code)) {
      return false;
    }
  }
  return true
}
