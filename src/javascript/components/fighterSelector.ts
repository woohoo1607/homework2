import {createElement} from '../helpers/domHelper';
import {renderArena} from './arena';
import versusImg from '../../../resources/versus.png';
import {createFighterPreview} from './fighterPreview';
import {fighterService} from "../services/fightersService";
import {IFighterDetails} from "../../types/interfaces";
import {FighterPosition} from "../../types/types";

export function createFightersSelector() {
  let selectedFighters: IFighterDetails[] = [];

  return async (event: Object, fighterId: string): Promise<void> => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ? playerOne : fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ? playerTwo : fighter : undefined;
    selectedFighters = [firstFighter as IFighterDetails, secondFighter as IFighterDetails];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map<string, IFighterDetails>();

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails | undefined> {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
  if (fighterDetailsMap.has(fighterId)) {
    return fighterDetailsMap.get(fighterId)
  } else {
    let fighterInfo = await fighterService.getFighterDetails(fighterId) as IFighterDetails;
    fighterDetailsMap.set(fighterId, fighterInfo);
    return fighterInfo
  }
}

function renderSelectedFighters(selectedFighters: IFighterDetails[]) {
  const fightersPreview = document.querySelector('.preview-container___root') as Element;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, FighterPosition.left);
  const secondPreview = playerTwo ? createFighterPreview(playerTwo, FighterPosition.right) : '';//change own code
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: IFighterDetails[]) {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: IFighterDetails[]) {
  renderArena(selectedFighters);
}
