import {createElement} from '../helpers/domHelper';
import {IFighter, IFighterDetails} from "../../types/interfaces";
import {FighterPosition} from "../../types/types";

export function createFighterPreview(fighter: IFighterDetails , position: FighterPosition) {
  const positionClassName = position === FighterPosition.right ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const imageElement = createFighterImage(fighter);
  const infoElement = createFighterInfo(fighter);
  fighterElement.append(imageElement,infoElement);

  return fighterElement;
}

export function createFighterImage(fighter: IFighter): HTMLElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterInfo(fighter: IFighterDetails) {
  const { name, health, attack, defense } = fighter;

  const infoElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info',
  });
  const nameElement = oneCharacteristic('Name', name);
  const healthElement = oneCharacteristic('Health', health);
  const attackElement = oneCharacteristic('Attack', attack);
  const defenseElement = oneCharacteristic('Defense', defense);

  infoElement.append(nameElement,healthElement,attackElement,defenseElement);
  return infoElement;
}
function oneCharacteristic (name: string, value: string | number) {
  const characteristicElement = createElement({
    tagName: 'p',
    className: `fighter-preview___${name} one-characteristic`,
  });
  const title = createElement({
    tagName: 'span',
    className: 'fighter-preview___characteristic-title',
  });
  const span = createElement({
    tagName: 'span',
    className: 'fighter-preview___characteristic-value',
  });
  title.textContent = name+':'
  span.textContent = value as string;
  characteristicElement.append(title, span);
  return characteristicElement
}
