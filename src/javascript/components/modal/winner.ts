import {showModal} from "./modal";
import {createElement} from "../../helpers/domHelper";
import {createFighterImage} from "../fighterPreview";
import App from "../../app";
import Fighter from "../Fighter";

export function showWinnerModal(fighter: Fighter) {
  // call showModal function
  const title = `${fighter.name} is Winner`;
  const imageElement = createFighterImage(fighter);
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  bodyElement.append(imageElement);
  showModal({title, bodyElement, onClose});
}

function onClose() {
  const root = document.getElementById('root') as HTMLElement;
  root.innerHTML = '';
  new App();
}
