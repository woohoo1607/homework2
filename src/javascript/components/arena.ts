import {createElement} from '../helpers/domHelper';
import {createFighterImage} from './fighterPreview';
import {fight} from "./fight";
import {showWinnerModal} from "./modal/winner";
import {IFighterDetails} from "../../types/interfaces";
import {FighterPosition} from "../../types/types";

export async function renderArena(selectedFighters: IFighterDetails[]): Promise<void> {
  const root = document.getElementById('root') as HTMLElement;
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  // todo:
  // - start the fight
  // - when fight is finished show winner
  let fighter = await fight(selectedFighters[0], selectedFighters[1]);
  showWinnerModal(fighter);
}

function createArena(selectedFighters: IFighterDetails[]) {
  const arena = createElement({ tagName: 'div', className: 'arena___root', attributes: {tabIndex: '0'}});
  const healthIndicators = createHealthIndicators(selectedFighters[0], selectedFighters[1]);
  const fighters = createFighters(selectedFighters[0], selectedFighters[1]);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: IFighterDetails, rightFighter: IFighterDetails) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, FighterPosition.left);
  const rightFighterIndicator = createHealthIndicator(rightFighter, FighterPosition.right);

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighterDetails, position: FighterPosition) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: IFighterDetails, secondFighter: IFighterDetails) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, FighterPosition.left);
  const secondFighterElement = createFighter(secondFighter, FighterPosition.right);

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighterDetails, position: FighterPosition) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === FighterPosition.right ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
