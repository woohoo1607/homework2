import {getHealthPercents} from "./fight";
import {IFighterDetails} from "../../types/interfaces";

class Fighter {
  _id: string;
  name: string;
  source: string;
  fullHealth: number;
  nowHealth: number;
  attack: number;
  defense: number;
  canHitCritical: boolean;
  delayCriticalHit: number;
  fighterHealthElement: HTMLElement;
    constructor({_id, name, health, attack, defense, source}: IFighterDetails, fighterHealthElement: string) {
        this._id = _id;
        this.name = name;
        this.fullHealth = health;
        this.nowHealth = health;
        this.attack = attack;
        this.defense = defense;
        this.source = source;
        this.canHitCritical = true;
        this.delayCriticalHit = 10000;
        this.fighterHealthElement = document.getElementById(fighterHealthElement) as HTMLElement;
    }
    blockCriticalAttack() {
        this.canHitCritical = false;
        setTimeout(() => {
            this.canHitCritical = true;
        }, this.delayCriticalHit)
    }

    changeHealth(damage: number) {
        this.nowHealth = this.nowHealth-damage;
        this.fighterHealthElement.style.width = getHealthPercents(this.fullHealth, this.nowHealth);
    }
}
export default Fighter;
