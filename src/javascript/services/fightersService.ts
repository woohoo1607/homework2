import { callApi } from '../helpers/apiHelper';
import {IFighter, IFighterDetails} from "../../types/interfaces";

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET') as IFighter[];

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string) {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET') as IFighterDetails | undefined;

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
