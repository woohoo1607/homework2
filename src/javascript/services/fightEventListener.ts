import {controls} from "../../constants/controls";
import {getCriticalHit, getDamage, isCriticalHit} from "../components/fight";
import Fighter from "../components/Fighter";

export function fightEventListener (resolve: Function, pressed: Set<string>, fighter1: Fighter, fighter2: Fighter) {
  document.addEventListener('keydown', function fightListener (event) {
    pressed.add(event.code);
    let damage;

    if (pressed.has(controls.PlayerOneAttack) && !pressed.has(controls.PlayerOneBlock) &&
        !pressed.has(controls.PlayerTwoBlock)
    ) {

      damage = getDamage(fighter1, fighter2);
      fighter2.changeHealth(damage);

    } else if (pressed.has(controls.PlayerTwoAttack) && !pressed.has(controls.PlayerTwoBlock) &&
              !pressed.has(controls.PlayerOneBlock)
    ) {

      damage = getDamage(fighter2, fighter1);
      fighter1.changeHealth(damage);

    } else if (isCriticalHit(pressed, controls.PlayerOneCriticalHitCombination) && fighter1.canHitCritical) {

      damage = getCriticalHit(fighter1);
      fighter2.changeHealth(damage);
      fighter1.blockCriticalAttack();

    } else if (isCriticalHit(pressed, controls.PlayerTwoCriticalHitCombination) && fighter2.canHitCritical) {

      damage = getCriticalHit(fighter2);
      fighter1.changeHealth(damage);
      fighter2.blockCriticalAttack();
    }

    if (fighter1.nowHealth<=0) {
      document.removeEventListener('keydown', fightListener)
      resolve(fighter2)
    }
    if (fighter2.nowHealth<=0) {
      document.removeEventListener('keydown', fightListener)
      resolve(fighter1)
    }

  });
}
