export interface IFighter {
    _id: string;
    name: string;
    source: string;
}

export interface IFighterDetails extends IFighter{
  health: number;
  attack: number;
  defense: number;
}

export interface IFighterList {
    [index: number]: IFighter;
}

export interface IControls {
  PlayerOneAttack: string;
  PlayerOneBlock: string;
  PlayerTwoAttack: string;
  PlayerTwoBlock: string;
  PlayerOneCriticalHitCombination: [string, string, string];
  PlayerTwoCriticalHitCombination: [string, string, string];
}

export interface ICreateElement {
  tagName: string;
  className?: string;
  attributes?: {[key: string]: string}
}

export interface IModal {
  title: string;
  bodyElement: HTMLElement;
  onClose(): void;
}
